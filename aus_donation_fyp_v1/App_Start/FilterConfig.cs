﻿using System.Web;
using System.Web.Mvc;

namespace aus_donation_fyp_v1
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
